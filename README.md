<div align="center">
  <img src="https://i.ibb.co/VmpbTvh/ic-logo.png" />
</div>

# Brains Store: A Google Playstore Client

**Brains Store** is an unofficial, FOSS client to Google's Play Store with an elegant design. Not only does Brains Store download, update, and search for apps like the Play Store, it also empowers the user with new features.

For those concerned with privacy, **Brains Store** does not require Google's proprietary framework (spyware?) to operate. It works perfectly fine with or without Google Play Services or [MicroG](https://microg.org/).

**Brains Store** was originally based on Sergei Yeriomin's [Yalp store](https://github.com/yeriomin/YalpStore). **Brains Store** v4.0 is a rewrite of version 3 in Kotlin that follows Material Design and runs on all devices running Android 4.4+. Read the roadmap page on our Wiki for more info!

## Features

- Free/Libre software — Has GPLv3 licence
- Beautiful design — Built upon latest Material Design guidelines
- Anonymous accounts — You can log in and download with anonymous accounts so you don't have to use your own account
- Personal account login — You can download purchased apps or access your wishlist by using your own Google account
- [Exodus](https://exodus-privacy.eu.org/) integration — Instantly see trackers an app is hiding in its code

## Project references

<details><summary>Open Source libraries Brains Store uses</summary>

- [RX-Java](https://github.com/ReactiveX/RxJava)
- [ButterKnife](https://github.com/JakeWharton/butterknife)
- [OkHttp3](https://square.github.io/okhttp/)
- [Glide](https://github.com/bumptech/glide)
- [Fetch2](https://github.com/tonyofrancis/Fetch)
- [GPlayApi](https://gitlab.com/momrn.game0/gplayapi)
- [PlayStoreApi-v2](https://github.com/whyorean/playstore-api-v2) (Deprecated! Used up till v3)

</details>

<details><summary>Brains Store is based on these projects</summary>

- [YalpStore](https://github.com/yeriomin/YalpStore)
- [AppCrawler](https://github.com/Akdeniz/google-play-crawler)
- [Raccoon](https://github.com/onyxbits/raccoon4)
- [SAI](https://github.com/Aefyr/SAI)

</details>
